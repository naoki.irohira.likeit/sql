create database usermanagement DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

use usermanagement;


create table user(
    id SERIAL primary key unique not null AUTO_INCREMENT,
	login_id varchar(255) unique not null,
	name varchar(255) not null,
    birth_date DATE not null,
    password varchar(255) not null,
    create_date DATETIME not null,
    update_date DATETIME not null
);

drop table user;

use usermanagement;

insert into user values (1, 'admin','�Ǘ���' ,'1990-07-30' , '1234','2020-07-01','2020-07-01') ;
